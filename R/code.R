#' @importFrom magrittr "%>%"

#' @title Extract class info from birch code
#'
#' @export
extract_class_info <- function(code) {
  m <- stringr::str_match_all(code, "class[[:space:]]+([A-z0-9]+)\\W+([A-z0-9]+)")
  list(
    filename = paste0("bi/", m[[1]][1,2], ".bi"),
    name = m[[1]][1,2],
    type = m[[1]][1,3],
    modelclass = stringr::str_detect(m[[1]][1,3], "Model"),
    code = code
  )
}

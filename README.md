# rbirch

R package for pMCMC inference of state-space models based on birch: http://birch-lang.org

# Installation

rbirch relies on [birch](birch-lang.org) for their SMC implementation, so you first need to install birch.

[Birch installation guide](http://birch-lang.org/getting-started/installing/)

Then it is easy to install rbirch using devtools:

```R
library(devtools)
# The pMCMC implementation depends on adaptive.mcmc from the fluEvidenceSynthesis package
install_github("MJomaba/flu-evidence-synthesis", dependencies = TRUE)
install_gitlab("BlackEdder/rbirch")
```

# Usage

For usage instructions please see the vignettes included with the package. The model itself needs to be implemented using the [birch](http://birch-lang.org) probabilistic language. Then rbirch can be used to process the data, call birch and extract the inference results. Birch only accepts json format data. We recommend the R package RJSONIO to convert your data to json.
